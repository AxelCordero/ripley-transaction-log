import {
  Table,
  Column,
  Model,
  PrimaryKey,
  AutoIncrement,
  DataType,
  NotEmpty,
  Default,
} from 'sequelize-typescript';

@Table({
  tableName: 'trx_log',
  timestamps: false,
})
export class TrxLog extends Model {
  @AutoIncrement
  @PrimaryKey
  @Column
  id_TRX: number;

  @Default(DataType.NOW)
  @Column({
    type: DataType.DATE,
    allowNull: false,
    field: 'fecha_TRX',
  })
  fecha_TRX: Date;

  @NotEmpty
  @Column({
    type: DataType.TEXT,
    allowNull: false,
    field: 'json_TRX',
  })
  json_TRX: string;
}
