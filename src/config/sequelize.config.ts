import { Injectable } from '@nestjs/common';
import {
  SequelizeOptionsFactory,
  SequelizeModuleOptions,
} from '@nestjs/sequelize';

@Injectable()
export class SequelizeConfigService implements SequelizeOptionsFactory {
  createSequelizeOptions(): SequelizeModuleOptions {
    return {
      dialect: 'mysql',
      host: process.env.MYSQL_URL,
      port: parseInt(process.env.MYSQL_PORT),
      username: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASS,
      database: process.env.MYSQL_BD,
      define: {
        timestamps: true,
        freezeTableName: true,
        paranoid: true,
      },
      pool: {
        max: 5,
        min: 0,
        idle: 10000,
      },
      autoLoadModels: true,
      synchronize: true,
    };
  }
}
