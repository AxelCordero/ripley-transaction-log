import { Test, TestingModule } from '@nestjs/testing';
import { TrxLogRepository } from './trx-log-repository';

describe('TrxLogRepository', () => {
  let provider: TrxLogRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TrxLogRepository],
    }).compile();

    provider = module.get<TrxLogRepository>(TrxLogRepository);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
