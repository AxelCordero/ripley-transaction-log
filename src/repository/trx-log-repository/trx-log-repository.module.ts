import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { TrxLog } from 'src/model/trx-log.model';
import { TrxLogRepository } from './trx-log-repository';

@Module({
  imports: [SequelizeModule.forFeature([TrxLog])],
  exports: [TrxLogRepository],
  providers: [TrxLogRepository],
})
export class TrxLogRepositoryModule {}
