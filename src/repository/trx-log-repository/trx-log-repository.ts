import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { InsertTrxLogDto } from 'src/interface/trx-log.interface';
import { TrxLog } from 'src/model/trx-log.model';

@Injectable()
export class TrxLogRepository {
  constructor(
    @InjectModel(TrxLog)
    private trxLogModel: typeof TrxLog,
  ) {}
  async saveTrxLog(log: InsertTrxLogDto) {
    try {
      await TrxLog.create({ ...log });
    } catch (error) {
      throw error;
    }
  }
}
