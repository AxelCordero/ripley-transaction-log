import { Module } from '@nestjs/common';
import { TrxLogRepositoryModule } from './trx-log-repository/trx-log-repository.module';

@Module({
  imports: [TrxLogRepositoryModule],
})
export class RepositoryModule {}
