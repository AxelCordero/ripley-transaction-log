import { Test, TestingModule } from '@nestjs/testing';
import { SaveLogController } from './save-log.controller';
import { SaveLogService } from './save-log.service';

describe('SaveLogController', () => {
  let controller: SaveLogController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SaveLogController],
      providers: [SaveLogService],
    }).compile();

    controller = module.get<SaveLogController>(SaveLogController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
