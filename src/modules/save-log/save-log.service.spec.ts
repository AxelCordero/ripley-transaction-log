import { Test, TestingModule } from '@nestjs/testing';
import { SaveLogService } from './save-log.service';

describe('SaveLogService', () => {
  let service: SaveLogService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SaveLogService],
    }).compile();

    service = module.get<SaveLogService>(SaveLogService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
