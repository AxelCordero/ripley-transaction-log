import { Module } from '@nestjs/common';
import { SaveLogService } from './save-log.service';
import { SaveLogController } from './save-log.controller';
import { TrxLogRepositoryModule } from 'src/repository/trx-log-repository/trx-log-repository.module';

@Module({
  imports: [TrxLogRepositoryModule],
  controllers: [SaveLogController],
  providers: [SaveLogService],
})
export class SaveLogModule {}
