import { BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { InsertTrxLogDto } from 'src/interface/trx-log.interface';
import { TrxLogRepository } from 'src/repository/trx-log-repository/trx-log-repository';

@Injectable()
export class SaveLogService {
  constructor(private trxLogRepository: TrxLogRepository) {}

  async saveTrxLog(trxLogData: InsertTrxLogDto) {
    try {
      await this.trxLogRepository.saveTrxLog(trxLogData);
      return {
        status: HttpStatus.OK,
        message: 'TRX_LOG SAVED',
      };
    } catch (error) {
      console.log(error);
      throw new BadRequestException(error.message);
    }
  }
}
