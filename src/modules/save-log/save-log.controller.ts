import { Body, Controller, Post } from '@nestjs/common';
import { SaveLogService } from './save-log.service';
import { InsertTrxLogDto } from 'src/interface/trx-log.interface';

@Controller('save-log')
export class SaveLogController {
  constructor(private readonly saveLogService: SaveLogService) {}

  @Post('')
  simulate(@Body() trxLogData: InsertTrxLogDto) {
    return this.saveLogService.saveTrxLog(trxLogData);
  }
}
